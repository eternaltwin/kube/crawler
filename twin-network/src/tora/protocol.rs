// See: https://github.com/HaxeFoundation/tora/blob/master/tora/Protocol.hx

use std::future::Future;
use std::ops::Range;
use std::sync::atomic::{AtomicBool, Ordering};

use tokio::io::{AsyncWriteExt, BufReader};
use tokio::net::{tcp, TcpStream};

use super::*;

pub async fn connect(addr: &ToraAddr) -> Result<(ToraReceiver, ToraSender)> {
    let sock = TcpStream::connect((addr.host.as_ref(), addr.port))
        .await
        .map_err(ToraError::ConnectionError)?;

    let (sock_receiver, sock_sender) = sock.into_split();
    let recv_errored = Arc::new(AtomicBool::new(false));
    let sender = ToraSender::new(addr.clone(), recv_errored.clone(), sock_sender);
    let receiver = ToraReceiver::new(sock_receiver, recv_errored);
    Ok((receiver, sender))
}

pub struct ToraSender {
    addr: ToraAddr,
    recv_errored: Arc<AtomicBool>,
    sender: Option<tcp::OwnedWriteHalf>,
    state: MessageState,
}

impl ToraSender {
    fn new(addr: ToraAddr, recv_errored: Arc<AtomicBool>, sender: tcp::OwnedWriteHalf) -> Self {
        let state = MessageState::new();
        Self {
            addr,
            recv_errored,
            state,
            sender: Some(sender),
        }
    }

    pub fn endpoint(&mut self, path: &str) -> MessageBuilder<'_> {
        self.state.reset();
        match self.state.add_host_and_endpoint(&self.addr.host, path) {
            Ok(_) => MessageBuilder(Ok(self)),
            Err(err) => MessageBuilder(Err(err)),
        }
    }

    async fn send(&mut self) -> Result<()> {
        if let Some(sender) = &mut self.sender {
            if self.recv_errored.load(Ordering::Acquire) {
                // The associated receiver errored and this
                // sender is now broken; drop it.
                // Any messages sent since recv_errored was
                // set to true are lost.
                self.sender.take();
            } else {
                // Associated receiver is still ok, send message.
                sender.write_all(&self.state.buf).await?;
                sender.flush().await?;
                return Ok(());
            }
        }
        Err(ToraError::IoError(io::Error::new(
            io::ErrorKind::Other,
            "broken writer",
        )))
    }

    pub(super) fn is_broken(&self) -> bool {
        self.recv_errored.load(Ordering::Acquire)
    }

    pub async fn close(self) -> Result<()> {
        if let Some(mut sender) = self.sender {
            sender.shutdown().await?;
            sender.forget();
        }
        Ok(())
    }
}

struct MessageParam {
    key: Range<usize>,
    value: Range<usize>,
}

struct MessageState {
    buf: Vec<u8>,
    params: Vec<MessageParam>,
}

impl MessageState {
    fn new() -> Self {
        Self {
            buf: Vec::new(),
            params: Vec::new(),
        }
    }

    fn reset(&mut self) {
        self.buf.clear();
        self.params.clear();
    }

    fn write_string(&mut self, string: &str, code: Code) -> BoxResult<Range<usize>> {
        let mut frame = FrameBuilder::begin(&mut self.buf, code);
        frame.push(string.as_bytes())?;
        let range = frame.content_range();
        frame.finish();
        Ok(range)
    }

    fn add_key_value<F>(
        &mut self,
        keycode: Code,
        valuecode: Code,
        name: &str,
        value_builder: F,
    ) -> BoxResult<()>
    where
        F: FnOnce(&mut FrameBuilder<'_>) -> BoxResult<()>,
    {
        let start = self.buf.len();
        let key = self.write_string(name, keycode)?;
        let mut frame = FrameBuilder::begin_with_discard(&mut self.buf, valuecode, start);
        value_builder(&mut frame)?;

        if valuecode == Code::ParamValue {
            let value = frame.content_range();
            self.params.push(MessageParam { key, value });
        }
        frame.finish();
        Ok(())
    }

    fn add_get_params(&mut self) -> BoxResult<()> {
        let mut tmp_buf = [0u8; 1024];

        let mut params = FrameBuilder::begin(&mut self.buf, Code::GetParams);
        for (i, param) in self.params.iter().enumerate() {
            if i > 0 {
                params.push(b";")?;
            }
            Self::copy_url_encoded(&mut params, param.key.clone(), &mut tmp_buf)?;
            params.push(b"=")?;
            Self::copy_url_encoded(&mut params, param.value.clone(), &mut tmp_buf)?;
        }
        params.finish();
        Ok(())
    }

    fn add_host_and_endpoint(&mut self, host: &str, endpoint: &str) -> BoxResult<()> {
        self.write_string(host, Code::HostResolve)?;

        let mut uri = FrameBuilder::begin(&mut self.buf, Code::Uri);
        if !endpoint.starts_with('/') {
            uri.push(b"/")?;
        }
        uri.push(endpoint.as_bytes())?;
        uri.finish();
        Ok(())
    }

    fn finish(&mut self) {
        FrameBuilder::begin(&mut self.buf, Code::Execute).finish()
    }

    fn copy_url_encoded(
        frame: &mut FrameBuilder<'_>,
        mut range: Range<usize>,
        tmp_buf: &mut [u8],
    ) -> BoxResult<()> {
        while range.start < range.end {
            let mid = std::cmp::min(range.start + tmp_buf.len(), range.end);
            let tmp = &mut tmp_buf[..(mid - range.start)];
            tmp.copy_from_slice(&frame.buf()[range.start..mid]);
            range = mid..range.end;

            for part in url::form_urlencoded::byte_serialize(tmp) {
                frame.push(part.as_bytes())?;
            }
        }
        Ok(())
    }

    fn steal_buffer(&mut self) -> Vec<u8> {
        std::mem::take(&mut self.buf)
    }
}

#[must_use = "this builder won't do anything unless a a terminal operation is called"]
pub struct MessageBuilder<'a>(BoxResult<&'a mut ToraSender>);

impl<'a> MessageBuilder<'a> {
    pub fn add_header<F>(mut self, name: &str, value_builder: F) -> Self
    where
        F: FnOnce(&mut FrameBuilder<'_>) -> BoxResult<()>,
    {
        self.0 = self.0.and_then(|p| {
            p.state
                .add_key_value(Code::HeaderKey, Code::HeaderValue, name, value_builder)?;
            Ok(p)
        });
        self
    }

    pub fn add_param<F>(mut self, name: &str, value_builder: F) -> Self
    where
        F: FnOnce(&mut FrameBuilder<'_>) -> BoxResult<()>,
    {
        self.0 = self.0.and_then(|p| {
            p.state
                .add_key_value(Code::ParamKey, Code::ParamValue, name, value_builder)?;
            Ok(p)
        });
        self
    }

    fn finish(self) -> Result<&'a mut ToraSender> {
        self.0
            .and_then(|p| {
                p.state.add_get_params()?;
                p.state.finish();
                Ok(p)
            })
            .map_err(ToraError::BuilderError)
    }

    pub fn send(self) -> impl Future<Output = Result<()>> + 'a {
        let result = self.finish();
        async { result?.send().await }
    }

    pub fn into_bytes(self) -> Result<Vec<u8>> {
        self.finish().map(|p| p.state.steal_buffer())
    }
}

pub struct ToraReceiver {
    reader: BufReader<tcp::OwnedReadHalf>,
    buf: Vec<u8>,
    cur_frame_end: Option<usize>,
    recv_errored: Arc<AtomicBool>,
}

impl ToraReceiver {
    fn new(receiver: tcp::OwnedReadHalf, recv_errored: Arc<AtomicBool>) -> Self {
        Self::new_with(receiver, Vec::new(), recv_errored)
    }

    fn new_with(receiver: tcp::OwnedReadHalf, buf: Vec<u8>, recv_errored: Arc<AtomicBool>) -> Self {
        Self {
            reader: BufReader::new(receiver),
            buf,
            cur_frame_end: None,
            recv_errored,
        }
    }

    pub(super) fn is_broken(&self) -> bool {
        self.recv_errored.load(Ordering::Acquire)
    }

    fn set_broken(&self) {
        self.recv_errored.store(true, Ordering::Release)
    }

    pub async fn advance(&mut self) -> Result<()> {
        if self.is_broken() {
            return Err(ToraError::IoError(io::Error::new(
                io::ErrorKind::Other,
                "broken receiver",
            )));
        }

        let mut pos = 0;
        self.cur_frame_end = None;
        loop {
            let (code, len) = match raw::read_frame(&mut self.reader, &mut self.buf, pos).await {
                Ok(Some(frame)) => frame,
                Ok(None) => return Ok(()),
                Err(err) => {
                    self.set_broken();
                    return Err(err.into());
                }
            };
            let end = pos + len;

            return match code {
                Code::HeaderKey | Code::HeaderValue | Code::HeaderAddValue | Code::Log => continue, // ignore
                Code::Listen | Code::Execute => {
                    self.cur_frame_end = Some(end);
                    Ok(()) // Done
                }
                Code::Error => {
                    let msg = String::from_utf8_lossy(&self.buf[pos..end]).to_string();
                    Err(ToraError::RemoteError(msg))
                }
                Code::Print => {
                    pos = end;
                    continue;
                }
                _ => Err(ToraError::UnsupportedCode(code)),
            };
        }
    }

    pub fn frame(&mut self) -> Option<&mut [u8]> {
        self.cur_frame_end.map(move |end| &mut self.buf[..end])
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_message_state<F: FnOnce(&mut MessageState)>(expected: &[u8], f: F) {
        let mut state = MessageState::new();
        f(&mut state);
        let buf = &state.buf[..];
        if buf != expected {
            panic!(
                "incorrect message:\nexpected: {:?}\n     got: {:?}",
                String::from_utf8_lossy(expected),
                String::from_utf8_lossy(buf),
            );
        }
    }

    fn add_header(state: &mut MessageState, name: &str, value: &str) {
        state
            .add_key_value(Code::HeaderKey, Code::HeaderValue, name, |f| {
                f.push(value.as_bytes())?;
                Ok(())
            })
            .unwrap();
    }

    fn add_param(state: &mut MessageState, name: &str, value: &str) {
        state
            .add_key_value(Code::ParamKey, Code::ParamValue, name, |f| {
                f.push(value.as_bytes())?;
                Ok(())
            })
            .unwrap();
    }

    macro_rules! tests_message_state {
        ($($name:ident($state:ident) $expr:expr => $string:expr;)+) => { $(
            #[test]
            fn $name() {
                test_message_state($string, |$state| $expr)
            }
        )+}
    }

    tests_message_state!(
        msg_simple(state) {
            add_header(state, "hello", "world");
            add_header(state, "foo", "bar");
        } => b"\x06\x05\x00\x00hello\x07\x05\x00\x00world\x06\x03\x00\x00foo\x07\x03\x00\x00bar";

        msg_get_params(state) {
            add_header(state, "hello", "world");
            add_param(state, "foo", "bar");
            add_param(state, "baz", "qu!ux");
            state.add_get_params().unwrap();
        } => concat!(
            "\x06\x05\x00\x00hello\x07\x05\x00\x00world\x09\x03\x00\x00foo\x0A\x03\x00\x00bar",
            "\x09\x03\x00\x00baz\x0A\x05\x00\x00qu!ux\x04\x13\x00\x00foo=bar;baz=qu%21ux",
        ).as_bytes();

        msg_discard_error(state) {
            add_header(state, "hello", "world");
            state.add_key_value(Code::ParamKey, Code::ParamValue, "huge", |f| {
                f.push(&vec![0; raw::MAX_MSG_LEN + 1])?;
                Ok(())
            }).unwrap_err();
            add_header(state, "foo", "bar");
        } => b"\x06\x05\x00\x00hello\x07\x05\x00\x00world\x06\x03\x00\x00foo\x07\x03\x00\x00bar";
    );
}
