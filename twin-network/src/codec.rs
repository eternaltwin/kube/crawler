use std::fmt;
use std::sync::Arc;

use thiserror::Error;

#[derive(Clone)]
pub struct CodecKey {
    full_key_utf16: Arc<[u16]>,
    key_start: usize,
    perm: Arc<[u8; Self::PERM_LEN]>,
}

pub type CodecCrc = [u8; CodecKey::CRC_LEN];

#[derive(Debug, Error)]
pub enum DecodeError {
    #[error("not enough bytes (< {})", CodecKey::CRC_LEN)]
    TooShort,
    #[error("invalid CRC")]
    InvalidCrc,
}

impl fmt::Debug for CodecKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        struct Utf16<'a>(&'a [u16]);
        impl<'a> fmt::Debug for Utf16<'a> {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                use fmt::Write;
                f.write_char('"')?;

                let chars = std::char::decode_utf16(self.0.iter().cloned())
                    .map(|c| c.unwrap_or(std::char::REPLACEMENT_CHARACTER))
                    .flat_map(|c| c.escape_debug());

                for c in chars {
                    f.write_char(c)?;
                }

                f.write_char('"')
            }
        }
        let (salt, key) = self.full_key_utf16.split_at(self.key_start);
        f.debug_struct("CodecKey")
            .field("key", &Utf16(key))
            .field("salt", &Utf16(salt))
            .finish()
    }
}

impl CodecKey {
    const PERM_LEN: usize = 256;
    const CRC_LEN: usize = 4;

    pub fn new(key: &str, salt: &str) -> Self {
        let mut full_key_utf16 = salt.encode_utf16().collect::<Vec<_>>();
        let key_start = full_key_utf16.len();
        full_key_utf16.extend(key.encode_utf16());
        let full_key_utf16 = Arc::from(full_key_utf16);
        let perm = Arc::new(Self::calculate_permutation(&full_key_utf16));
        CodecKey {
            full_key_utf16,
            key_start,
            perm,
        }
    }

    fn calculate_permutation(full_key: &[u16]) -> [u8; Self::PERM_LEN] {
        let mut buf = [0u8; 256];
        for (i, b) in buf.iter_mut().enumerate() {
            *b = i as u8 & 0x7F;
        }

        let mut j = 0usize;
        for i in 0..buf.len() {
            j += usize::from(buf[i]) + usize::from(full_key[i % full_key.len()]);
            j &= 0x7F;

            buf.swap(i, j);
        }

        buf
    }

    pub fn encode(&self, buf: &mut Vec<u8>) {
        let crc = self.encode_slice(buf);
        buf.extend_from_slice(&crc);
    }

    pub fn encode_slice(&self, buf: &mut [u8]) -> CodecCrc {
        let crc = self.encode_in_place(buf);
        Self::encode_crc(crc)
    }

    pub fn decode(&self, buf: &mut Vec<u8>) -> Result<(), DecodeError> {
        let len = self.decode_slice(&mut buf[..])?.len();
        buf.truncate(len);
        Ok(())
    }

    pub fn decode_slice<'a>(&self, bytes: &'a mut [u8]) -> Result<&'a mut [u8], DecodeError> {
        let (bytes, crc_bytes) = bytes.split_at_mut(bytes.len().saturating_sub(Self::CRC_LEN));

        let crc = Self::encode_crc(self.decode_in_place(bytes));
        if crc_bytes.len() != Self::CRC_LEN {
            Err(DecodeError::TooShort)
        } else if crc != crc_bytes {
            // "Encode" the buffer to return it to its original state.
            self.encode_in_place(bytes);
            Err(DecodeError::InvalidCrc)
        } else {
            Ok(bytes)
        }
    }

    fn encode_in_place(&self, bytes: &mut [u8]) -> u32 {
        let mut crc_a: u32 = self.perm[0].into();
        let mut crc_b: u32 = self.perm[1].into();

        for (i, b) in bytes.iter_mut().enumerate() {
            let tmp = *b ^ self.perm[i % Self::PERM_LEN];
            *b = if tmp == 0 { *b } else { tmp };
            crc_a = (crc_a + u32::from(tmp)) % 65521;
            crc_b = (crc_b + crc_a) % 65521;
        }

        crc_a ^ (crc_b << 8)
    }

    fn encode_crc(crc: u32) -> [u8; Self::CRC_LEN] {
        const BASE64_CHARS: &[u8] =
            b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
        [
            BASE64_CHARS[(crc & 63) as usize],
            BASE64_CHARS[((crc >> 6) & 63) as usize],
            BASE64_CHARS[((crc >> 12) & 63) as usize],
            BASE64_CHARS[((crc >> 18) & 63) as usize],
        ]
    }

    fn decode_in_place(&self, bytes: &mut [u8]) -> u32 {
        let mut crc_a: u32 = self.perm[0].into();
        let mut crc_b: u32 = self.perm[1].into();

        for (i, b) in bytes.iter_mut().enumerate() {
            let tmp = *b ^ self.perm[i % Self::PERM_LEN];
            let c = if tmp == 0 { 0 } else { *b };
            *b = if tmp == 0 { *b } else { tmp };
            crc_a = (crc_a + u32::from(c)) % 65521;
            crc_b = (crc_b + crc_a) % 65521;
        }

        crc_a ^ (crc_b << 8)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn roundtrip() {
        let key = CodecKey::new("key", "salt");
        let msg = "Hello world! This is a test message!";

        let mut buf = msg.as_bytes().to_vec();
        key.encode(&mut buf);
        key.decode(&mut buf).unwrap();
        assert_eq!(msg.as_bytes(), &buf[..])
    }

    #[test]
    fn error_rollback() {
        let key = CodecKey::new("key", "salt");
        let msg = "Hello world! This is not an encoded message!";

        let mut buf = msg.as_bytes().to_vec();
        key.decode(&mut buf).unwrap_err();
        assert_eq!(msg.as_bytes(), &buf[..])
    }
}
