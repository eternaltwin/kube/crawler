use std::cmp::Ordering;

use crate::errors::DbOpenError;
use crate::migrations;
use crate::KubeDb;

pub struct Config {
    pub user: String,
    pub password: String,
    pub host: String,
    pub port: u16,
    pub name: String,
}

impl Config {
    async fn connect(mut self) -> std::result::Result<sqlx::PgPool, DbOpenError> {
        if !self.name.starts_with('/') {
            self.name.insert(0, '/');
        }

        let mut url =
            url::Url::parse("postgresql://").map_err(|e| sqlx::Error::Configuration(e.into()))?;
        url.set_host(Some(&self.host))
            .map_err(|e| sqlx::Error::Configuration(e.into()))?;
        url.set_port(Some(self.port)).expect("couldn't create url");
        url.set_path(&self.name);

        eprintln!("Connecting to {}...", url); // TODO: use the log or tracing crate

        // IMPORTANT: set username and password AFTER logging the url
        url.set_username(&self.user).expect("couldn't create url");
        url.set_password(Some(&self.password))
            .expect("couldn't create url");

        let db = sqlx::postgres::PgPoolOptions::new()
            .acquire_timeout(std::time::Duration::from_millis(1000))
            .connect(url.as_str())
            .await?;
        Ok(db)
    }

    pub async fn open(self) -> std::result::Result<KubeDb, DbOpenError> {
        let db = self.connect().await?;
        let version = migrations::detect_database_schema_version(&db).await?;
        match version {
            Some(v) if v == migrations::CURRENT_SCHEMA_VERSION => Ok(KubeDb(db)),
            Some(v) => Err(DbOpenError::UnsupportedSchema(v)),
            None => Err(DbOpenError::UnknownSchema),
        }
    }

    pub async fn upgrade(self, dry_run: bool) -> std::result::Result<KubeDb, DbOpenError> {
        let db = self.connect().await?;
        let version = migrations::detect_database_schema_version(&db).await?;

        match version
            .unwrap_or(0)
            .cmp(&migrations::CURRENT_SCHEMA_VERSION)
        {
            Ordering::Greater => return Err(DbOpenError::UnsupportedSchema(version.unwrap())),
            // TODO: use the log or tracing crate
            Ordering::Equal => eprintln!("The database is already up-to-date; nothing to do!"),
            Ordering::Less => {
                let status = if dry_run {
                    migrations::dry_run_migrations(&db, version).await
                } else {
                    migrations::run_migrations(&db, version).await
                };
                status.map_err(DbOpenError::MigrationError)?;
            }
        }

        Ok(KubeDb(db))
    }
}
