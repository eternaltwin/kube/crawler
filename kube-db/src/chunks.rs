use chrono::{DateTime, Utc};
use futures::prelude::*;

use kube_core::region::{ChunkBytes, ChunkPos, CompressionMethod};

use crate::{DbConn, DbPool, Result};

const COMPRESSION_METHOD: CompressionMethod = CompressionMethod::RleDeflate;

pub struct DbChunk {
    pub cx: i32,
    pub cy: i32,
    pub data: Option<ChunkBytes>,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Copy, Clone, sqlx::FromRow)]
pub struct ChunkStatus {
    pub cx: i32,
    pub cy: i32,
    pub generated: bool,
    pub created_at: DateTime<Utc>,
    pub last_checked_at: DateTime<Utc>,
}

#[derive(Debug, Copy, Clone)]
pub enum ChunkUpdate {
    Unchanged,
    Changed,
    Created,
    Deleted,
}

pub fn get_chunks_status(db: &DbPool) -> impl Stream<Item = Result<ChunkStatus>> + '_ {
    sqlx::query_as!(
        ChunkStatus,
        r#"
        SELECT cx, cy, data IS NOT NULL as "generated!", created_at, last_checked_at
        FROM chunks ORDER BY (cx, cy);
    "#
    )
    .fetch(db)
    .map_err(|err| err.into())
}

pub fn get_uncompressed_chunks(db: &DbPool) -> impl Stream<Item = Result<ChunkPos>> + '_ {
    sqlx::query!(
        r#"
        SELECT cx, cy FROM chunks WHERE NOT compressed
        ORDER BY (cx, cy);
    "#
    )
    .fetch(db)
    .map(|raw| {
        let raw = raw?;
        Ok((raw.cx, raw.cy))
    })
}

struct ChunkVersion {
    data: Option<Vec<u8>>,
    compressed: bool,
    is_delta: bool,
    valid_since: DateTime<Utc>,
}

fn compute_chunk_from_versions(version_list: Vec<ChunkVersion>) -> Result<Option<ChunkBytes>> {
    let mut result: Option<ChunkBytes> = None;
    // Temporary buffer for decompressing versions.
    let mut tmp_buf: Option<ChunkBytes> = None;

    let is_sorted = version_list
        .windows(2)
        .all(|e| e[0].valid_since <= e[1].valid_since);
    if !is_sorted {
        panic!("chunk versions must be provided in chronological order");
    }

    // Apply versions in reverse chronological order
    for version in version_list.into_iter().rev() {
        let bytes = match version.data {
            Some(enc) => enc,
            None => {
                // Wipe the current data
                tmp_buf = result.take().and(tmp_buf);
                continue;
            }
        };

        // Extract the current version data, using
        // the temporary buffer if possible.
        let version_data = if version.compressed {
            if let Some(mut buf) = tmp_buf.take() {
                buf.decompress_in_place(&bytes)?;
                buf
            } else {
                ChunkBytes::decompress(&bytes)?
            }
        } else {
            ChunkBytes::from_vec(bytes)?
        };

        // Apply the current version
        if let Some(data) = result.as_mut() {
            if version.is_delta {
                data.delta_in_place(&version_data);
                tmp_buf = Some(version_data);
            } else {
                tmp_buf = Some(std::mem::replace(data, version_data));
            }
        } else {
            // TODO: return an error if is_delta is true?
            result = Some(version_data);
        }
    }
    Ok(result)
}

pub async fn get_chunk(
    db: &DbPool,
    pos: (i32, i32),
    date: Option<DateTime<Utc>>,
) -> Result<Option<DbChunk>> {
    let chunk_versions = if let Some(date) = date {
        // Get all chunk versions applicable for the given date.
        let mut chunk_stream = sqlx::query_as!(ChunkVersion, r#"
            WITH selected_chunk AS (
                SELECT * FROM chunks_full_history WHERE cx = $1 AND cy = $2
            ), first_valid AS (
                SELECT MAX(valid_since) AS first_valid
                FROM selected_chunk WHERE valid_since <= $3
            )
            SELECT data, compressed as "compressed!", is_delta as "is_delta!", valid_since as "valid_since!"
            FROM selected_chunk, first_valid
            WHERE valid_since >= first_valid
            ORDER BY valid_since;
        "#, pos.0, pos.1, date).fetch(db);

        // We only need versions until the first non-delta.
        let mut chunks = Vec::new();
        while let Some(chunk) = chunk_stream.try_next().await? {
            let is_delta = chunk.is_delta;
            chunks.push(chunk);
            if !is_delta {
                break;
            }
        }

        chunks
    } else {
        // We want the current version, so grab it directly.
        sqlx::query!(
            r#"
            SELECT data, compressed, created_at FROM chunks
            WHERE cx = $1 AND cy = $2;
        "#,
            pos.0,
            pos.1
        )
        .fetch_optional(db)
        .await?
        .map(|raw| ChunkVersion {
            data: raw.data,
            compressed: raw.compressed,
            is_delta: false,
            valid_since: raw.created_at,
        })
        .into_iter()
        .collect()
    };

    if chunk_versions.is_empty() {
        return Ok(None);
    }

    tokio::task::block_in_place(|| {
        // The date of creation is the one of the oldest applicable version
        let created_at = chunk_versions
            .iter()
            .map(|v| v.valid_since)
            .min()
            .ok_or(sqlx::Error::RowNotFound)?;

        Ok(Some(DbChunk {
            cx: pos.0,
            cy: pos.1,
            data: compute_chunk_from_versions(chunk_versions)?,
            created_at,
        }))
    })
}

fn compute_version_from_chunk(
    current: ChunkVersion,
    new_chunk: Option<&ChunkBytes>,
) -> Result<(Option<ChunkVersion>, ChunkUpdate)> {
    assert!(!current.is_delta);

    let old_valid_since = current.valid_since;
    let make_version = |data: Option<Vec<u8>>, is_delta: bool| -> Option<ChunkVersion> {
        Some(ChunkVersion {
            data,
            is_delta,
            compressed: true,
            valid_since: old_valid_since,
        })
    };

    match (current.data, new_chunk) {
        (Some(mut old), None) => {
            if !current.compressed {
                old = ChunkBytes::from_vec(old)?.compress(COMPRESSION_METHOD)
            };
            Ok((make_version(Some(old), false), ChunkUpdate::Created))
        }

        (None, Some(_)) => Ok((make_version(None, false), ChunkUpdate::Deleted)),

        (Some(old), Some(new)) => {
            // Decompress old chunk data, and grab a buffer to use when recompressing.
            let (old, mut buf) = if current.compressed {
                (ChunkBytes::decompress(&old)?, old)
            } else {
                (ChunkBytes::from_vec(old)?, Vec::new())
            };

            // Compare uncompressed data, and compute the delta
            if &old == new {
                Ok((None, ChunkUpdate::Unchanged))
            } else {
                let mut delta = old;
                delta.delta_in_place(new);
                buf.clear();
                delta.compress_into(&mut buf, COMPRESSION_METHOD);
                Ok((make_version(Some(buf), true), ChunkUpdate::Changed))
            }
        }

        (None, None) => Ok((None, ChunkUpdate::Unchanged)),
    }
}

pub async fn save_chunk(db: &mut DbConn, chunk: &DbChunk) -> Result<ChunkUpdate> {
    // Try to insert a new chunk in the table (with dummy values, for now).
    // If the chunk already exists, retrieve its data.
    let old_chunk = loop {
        let is_new = sqlx::query!(
            r#"
            INSERT INTO chunks(cx, cy, data, compressed, created_at, last_checked_at)
            VALUES ($1, $2, NULL, TRUE, $3, $3)
            ON CONFLICT DO NOTHING;
        "#,
            chunk.cx,
            chunk.cy,
            chunk.created_at
        )
        .execute(&mut *db)
        .await?
        .rows_affected()
            > 0;

        if is_new {
            // This is a new chunk, put in the actual chunk data and return.
            // The new row isn't visible outside this transaction, so nobody
            // can delete it.
            if let Some(new_data) = chunk.data.as_ref() {
                let compressed =
                    tokio::task::block_in_place(|| new_data.compress(COMPRESSION_METHOD));

                sqlx::query!(
                    r#"
                    UPDATE chunks SET data = $3
                    WHERE cx = $1 AND cy = $2;
                "#,
                    chunk.cx,
                    chunk.cy,
                    compressed
                )
                .execute(&mut *db)
                .await?;
            }

            return Ok(ChunkUpdate::Created);
        }

        // Retrieve the current chunk data and lock the row.
        let chunk = sqlx::query!(
            r#"
            SELECT data, compressed, created_at FROM chunks
            WHERE cx = $1 AND cy = $2
            FOR UPDATE;
        "#,
            chunk.cx,
            chunk.cy
        )
        .fetch_optional(&mut *db)
        .await?;
        match chunk {
            Some(chunk) => break chunk,
            None => continue, // Somebody just deleted the row; retry
        }
    };

    // Check that the new data isn't outdated.
    if chunk.created_at < old_chunk.created_at {
        return Err(crate::Error::OutdatedData {
            outdated: chunk.created_at,
            saved: old_chunk.created_at,
        });
    }

    // Compute the version to add in the history table.
    let (old_version, update_kind, new_compressed) = tokio::task::block_in_place(|| {
        let new_chunk = chunk.data.as_ref();
        compute_version_from_chunk(
            ChunkVersion {
                data: old_chunk.data,
                compressed: old_chunk.compressed,
                is_delta: false,
                valid_since: old_chunk.created_at,
            },
            new_chunk,
        )
        .map(|(old, update)| {
            let new = new_chunk.map(|new| new.compress(COMPRESSION_METHOD));
            (old, update, new)
        })
    })?;

    if let Some(old_version) = old_version {
        // The chunk data changed: add the old version to the history
        // table and replace the current data.
        sqlx::query!(
            r#"
            INSERT INTO chunks_history(cx, cy, data, compressed, is_delta, valid_since)
            VALUES ($1, $2, $3, $4, $5, $6);
        "#,
            chunk.cx,
            chunk.cy,
            old_version.data,
            old_version.compressed,
            old_version.is_delta,
            old_version.valid_since
        )
        .execute(&mut *db)
        .await?;

        sqlx::query!(
            r#"
            UPDATE chunks
            SET data = $3, compressed = TRUE, created_at = $4, last_checked_at = $4
            WHERE cx = $1 AND cy = $2;
        "#,
            chunk.cx,
            chunk.cy,
            new_compressed,
            chunk.created_at
        )
        .execute(&mut *db)
        .await?;
    } else {
        // The data didn't change: update the last_checked_at column without modifying anything.
        sqlx::query!(
            r#"
            UPDATE chunks SET last_checked_at = $3
            WHERE cx = $1 AND cy = $2;
        "#,
            chunk.cx,
            chunk.cy,
            chunk.created_at
        )
        .execute(&mut *db)
        .await?;
    }

    Ok(update_kind)
}

pub async fn compress_chunk(db: &mut DbConn, pos: ChunkPos) -> Result<()> {
    let chunk = sqlx::query!(
        r#"
        SELECT cx, cy, data, compressed FROM chunks
        WHERE cx = $1 AND cy = $2
        FOR UPDATE;
    "#,
        pos.0,
        pos.1
    )
    .fetch_one(&mut *db)
    .await?;

    let data = match chunk.data {
        Some(data) if !chunk.compressed => ChunkBytes::from_vec(data)?,
        _ => return Ok(()), // Data already compressed, nothing to do
    };

    let compressed = tokio::task::block_in_place(|| data.compress(COMPRESSION_METHOD));


    sqlx::query!(
        r#"
        UPDATE chunks
        SET data = $1, compressed = TRUE
        WHERE cx = $2 AND cy = $3;
    "#,
        compressed,
        chunk.cx,
        chunk.cy
    )
    .execute(db)
    .await?;
    Ok(())
}
