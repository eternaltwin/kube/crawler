
-- Add `compressed` column to `chunks`.

ALTER TABLE chunks ADD COLUMN compressed BOOLEAN;
UPDATE chunks SET compressed = FALSE;
UPDATE chunks SET compressed = TRUE WHERE data IS NULL;
ALTER TABLE chunks ALTER COLUMN compressed SET NOT NULL;
ALTER TABLE chunks ADD CONSTRAINT chk_compressed CHECK (
    data IS NOT NULL OR compressed
);
