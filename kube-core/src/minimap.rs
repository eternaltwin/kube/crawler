use image::{GenericImage, Rgb};

use crate::block::{Block, BlockColor};
use crate::region::{Dimensions, PlaneStorage, RegionIdx, RegionStorage};

#[derive(Debug, Copy, Clone)]
pub struct AxisOrientation {
    pub x_inverted: bool,
    pub y_inverted: bool,
}

#[derive(Copy, Clone)]
pub struct HeightMapCol {
    block: Block,
    height: u8,
}

impl Default for HeightMapCol {
    #[inline]
    fn default() -> Self {
        Self {
            block: Block::Empty,
            height: 0,
        }
    }
}

impl HeightMapCol {
    fn add_block_on_top(&mut self, (_, _, z): RegionIdx, block: &Block) {
        if block.map_color() != BlockColor::Invisible {
            self.block = *block;
            self.height = z as u8;
        }
    }
}

pub type HeightMap<D> = PlaneStorage<HeightMapCol, D>;

impl<D: Dimensions> HeightMap<D> {
    pub fn from_region(region: &RegionStorage<Block, D>) -> Self {
        region.flatten(HeightMapCol::add_block_on_top)
    }

    pub fn fill_from(&mut self, region: &RegionStorage<Block, D>) {
        for (_, col) in self.blocks_mut() {
            *col = HeightMapCol::default();
        }
        region.flatten_in(self, HeightMapCol::add_block_on_top);
    }

    pub fn render<I>(&self, img: &mut I, offset_x: u8, offset_y: u8, orientation: AxisOrientation)
    where
        I: GenericImage<Pixel = Rgb<u8>>,
    {
        // TODO: check overflow into u32
        let width = std::cmp::min(img.width(), self.dims().width() as u32 - offset_x as u32);
        let height = std::cmp::min(img.height(), self.dims().depth() as u32 - offset_y as u32);
        let mut img = img.sub_image(0, 0, width, height);

        macro_rules! image_loop {
            ($img_x:ident => $x_expr:expr, $img_y:ident => $y_expr:expr) => {
                for $img_y in 0..height {
                    let y = $y_expr as usize + offset_y as usize;
                    for $img_x in 0..width {
                        let x = $x_expr as usize + offset_x as usize;

                        let info = self[(x, y)];
                        let color = info.block.map_color().to_rgb(info.height as usize);
                        img.put_pixel($img_x, $img_y, color);
                    }
                }
            };
        }

        match (orientation.x_inverted, orientation.y_inverted) {
            (false, false) => image_loop!(
                img_x => img_x as usize,
                img_y => img_y as usize
            ),
            (false, true) => image_loop!(
                img_x => width - img_x - 1,
                img_y => img_y
            ),
            (true, false) => image_loop!(
                img_x => img_x,
                img_y => height - img_y - 1
            ),
            (true, true) => image_loop!(
                img_x => width - img_x - 1,
                img_y => height - img_y - 1
            ),
        }
    }
}
