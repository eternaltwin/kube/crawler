mod compress;
mod dimensions;
mod rle;
mod storage;

use crate::block::Block;
use thiserror::Error;

pub use compress::CompressionMethod;
pub use dimensions::{ChunkDim, DynDim};
pub use storage::{Dimensions, PlaneIdx, PlaneStorage, RegionIdx, RegionStorage};

pub type ChunkPos = (i32, i32);
pub type PlanePos = (i64, i64);
pub type BlockPos = (i64, i64, i64);
pub type ChunkBytes = RegionStorage<u8, ChunkDim>;

pub const CHUNK_WIDTH: usize = 256;
pub const CHUNK_HEIGHT: usize = 32;
pub const CHUNK_SIZE: usize = CHUNK_WIDTH * CHUNK_WIDTH * CHUNK_HEIGHT;
pub const ZONES_PER_CHUNK: usize = 8;
pub const ZONE_WIDTH: usize = CHUNK_WIDTH / ZONES_PER_CHUNK;

#[derive(Debug, Error)]
pub enum RegionError {
    #[error("invalid compressed data for region: {0}")]
    Decompress(#[source] std::io::Error),
    #[error("invalid size for region: {}, expected {}",
        if let Some(actual) = .actual {
            actual.to_string()
        } else {
            format!("> {}", .expected)
        }, .expected)]
    InvalidSize {
        expected: usize,
        actual: Option<usize>,
    },
}

impl<D: Dimensions> RegionStorage<Block, D> {
    pub fn as_bytes(&self) -> &[u8] {
        Block::as_bytes(&self.data)
    }

    pub fn into_bytes(self) -> RegionStorage<u8, D> {
        RegionStorage {
            data: Block::into_bytes(self.data.into_vec()).into_boxed_slice(),
            dims: self.dims,
        }
    }
}

impl<D: Dimensions> RegionStorage<u8, D> {
    pub fn into_blocks(self) -> RegionStorage<Block, D> {
        RegionStorage {
            data: Block::from_bytes(self.data.into_vec()).into_boxed_slice(),
            dims: self.dims,
        }
    }

    pub fn delta_with(&self, other: &Self) -> Self {
        let mut dst = self.clone();
        dst.delta_in_place(other);
        dst
    }

    pub fn delta_in_place(&mut self, other: &Self) {
        if self.dims.dims() != other.dims.dims() {
            panic!(
                "can't delta, dims are different: {:?} != {:?}",
                self.dims.dims(),
                other.dims.dims()
            );
        }

        // Slices have the same length, but this helps the optimizer.
        let len = std::cmp::min(self.data.len(), other.data.len());
        let dst = &mut self.data[..len];
        let src = &other.data[..len];

        for i in 0..len {
            dst[i] ^= src[i];
        }
    }

    pub fn compress(&self, method: CompressionMethod) -> Vec<u8> {
        let mut buf = Vec::new();
        self.compress_into(&mut buf, method);
        buf
    }

    pub fn compress_into(&self, buf: &mut Vec<u8>, method: CompressionMethod) {
        compress::compress_into(&self.data, buf, method)
    }

    pub fn decompress(compressed: &[u8]) -> Result<Self, RegionError>
    where
        D: Default,
    {
        Self::decompress_sized(D::default(), compressed)
    }

    pub fn decompress_sized(dims: D, compressed: &[u8]) -> Result<Self, RegionError> {
        let mut chunk = Self::new_sized(dims);
        chunk.decompress_in_place(compressed)?;
        Ok(chunk)
    }

    pub fn decompress_in_place(&mut self, compressed: &[u8]) -> Result<(), RegionError> {
        compress::decompress_in_place(&mut self.data, compressed)
    }
}
