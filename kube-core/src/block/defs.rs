macro_rules! expand_all_blocks {
    ($($input:tt)*) => { __expand_impl! {
        items = {
            (  0, Empty         , "Air"                 , Invisible                         )
            (  1, Fixed         , "Béton"               , Shaded(   Rgb([145, 134, 135]))   )
            (  2, Water         , "Eau"                 , Fixed(    Rgb([57 , 168, 198]))   )
            (  3, SoilTree      , "Terre Brune"         , Shaded(   Rgb([181, 128, 58 ]))   )
            (  4, SoilTree1     , "Bois Bouleau"        , Fixed(    Rgb([110, 75 , 45 ]))   )
            (  5, SoilTree2     , "Feuilles Bouleau"    , Fixed(    Rgb([138, 118, 22 ]))   )
            (  6, AutumnTree    , "Terre d'Automne"     , Shaded(   Rgb([204, 121, 47 ]))   )
            (  7, AutumnTree1   , "Bois d'Automne"      , Fixed(    Rgb([204, 169, 132]))   )
            (  8, AutumnTree2   , "Feuilles d'Automne"  , Fixed(    Rgb([151, 98 , 41 ]))   )
            (  9, HighTree      , "Terre Sombre"        , Shaded(   Rgb([61 , 82 , 30 ]))   )
            ( 10, HighTree1     , "Bois Sombre"         , Fixed(    Rgb([110, 75 , 45 ]))   )
            ( 11, HighTree2     , "Feuilles Sombres"    , Fixed(    Rgb([80 , 140, 59 ]))   )
            ( 12, HighTree3     , "Herbes Sombres"      , Fixed(    Rgb([59 , 75 , 0  ]))   )
            ( 13, Clouds        , "Terre Bleue"         , Shaded(   Rgb([65 , 140, 152]))   )
            ( 14, Clouds1       , "Coeur de Nuage"      , Fixed(    Rgb([232, 238, 237]))   )
            ( 15, Clouds2       , "Nuage"               , Fixed(    Rgb([199, 210, 212]))   )
            ( 16, SphereTree    , "Bois Clair"          , Shaded(   Rgb([196, 169, 143]))   )
            ( 17, SphereTree1   , "Feuilles Roses"      , Fixed(    Rgb([153, 66 , 122]))   )
            ( 18, LargeTree     , "Racines"             , Shaded(   Rgb([207, 139, 92 ]))   )
            ( 19, LargeTree1    , "Bois Géant"          , Fixed(    Rgb([172, 86 , 43 ]))   )
            ( 20, LargeTree2    , "Feuilles Géantes"    , Fixed(    Rgb([185, 66 , 0  ]))   )
            ( 21, SnowSapin     , "Terre-Neige"         , Shaded(   Rgb([238, 238, 235]))   )
            ( 22, SnowSapin1    , "Bois Sapin"          , Fixed(    Rgb([110, 75 , 45 ]))   )
            ( 23, SnowSapin2    , "Feuilles-Neige"      , Fixed(    Rgb([238, 238, 236]))   )
            ( 24, Jungle        , "Jungle"              , Shaded(   Rgb([105, 142, 35 ]))   )
            ( 25, Jungle1       , "Lianes Sombres"      , Fixed(    Rgb([24 , 44 , 0  ]))   )
            ( 26, Jungle2       , "Lianes Claires"      , Fixed(    Rgb([80 , 140, 59 ]))   )
            ( 27, Jungle3       , "Lianes Pourries"     , Fixed(    Rgb([109, 120, 49 ]))   )
            ( 28, Caverns       , "Mur"                 , Shaded(   Rgb([86 , 79 , 75 ]))   )
            ( 29, Caverns1      , "Roc"                 , Fixed(    Rgb([101, 70 , 35 ]))   )
            ( 30, Caverns2      , "Pilier"              , Fixed(    Rgb([127, 94 , 61 ]))   )
            ( 31, Field         , "Prairie"             , Shaded(   Rgb([78 , 100, 23 ]))   )
            ( 32, Field1        , "Herbes Claires"      , Fixed(    Rgb([177, 178, 104]))   )
            ( 33, Field2        , "Herbes"              , Fixed(    Rgb([154, 141, 25 ]))   )
            ( 34, Swamp         , "Marais"              , Shaded(   Rgb([71 , 100, 44 ]))   )
            ( 35, Swamp1        , "Bois Marrais"        , Fixed(    Rgb([196, 161, 85 ]))   )
            ( 36, Swamp2        , "Feuilles Marais"     , Fixed(    Rgb([123, 107, 75 ]))   )
            ( 37, Swamp3        , "Boue"                , Fixed(    Rgb([114, 128, 74 ]))   )
            ( 38, SoilPeaks     , "Roche Froide"        , Shaded(   Rgb([89 , 68 , 57 ]))   )
            ( 39, SoilPeaks1    , "Roche Volcanique"    , Shaded(   Rgb([43 , 31 , 25 ]))   )
            ( 40, PilarPlain    , "Cendres"             , Shaded(   Rgb([166, 148, 142]))   )
            ( 41, PilarPlain1   , "Grès"                , Shaded(   Rgb([166, 154, 151]))   )
            ( 42, PilarPlain2   , "Totem Antique"       , Fixed(    Rgb([123, 110, 102]))   )
            ( 43, FlowerPlain   , "Pâturages"           , Shaded(   Rgb([72 , 112, 56 ]))   )
            ( 44, FlowerPlain1  , "Fleur Blanche"       , Fixed(    Rgb([173, 171, 90 ]))   )
            ( 45, FlowerPlain2  , "Fleur Rose"          , Fixed(    Rgb([155, 105, 102]))   )
            ( 46, FlowerPlain3  , "Stonehenge"          , Fixed(    Rgb([140, 143, 147]))   )
            ( 47, Savana        , "Savane"              , Shaded(   Rgb([175, 145, 65 ]))   )
            ( 48, Savana1       , "Arbuste Sec"         , Fixed(    Rgb([217, 185, 114]))   )
            ( 49, Savana2       , "Zèbre"               , Fixed(    Rgb([105, 102, 102]))   )
            ( 50, Savana3       , "Eléphant"            , Fixed(    Rgb([113, 89 , 76 ]))   )
            ( 51, Desert        , "Sable"               , Shaded(   Rgb([199, 144, 97 ]))   )
            ( 52, Desert1       , "Cactus"              , Fixed(    Rgb([86 , 115, 45 ]))   )
            ( 53, Fruit         , "Gland"               , Fixed(    Rgb([157, 113, 1  ]))   )
            ( 54, Lava          , "Lave"                , Shaded(   Rgb([200, 53 , 0  ]))   )
            ( 55, Dolpart       , "Sombre"              , Fixed(    Rgb([41 , 35 , 32 ]))   )
            ( 56, Dolmen        , "Dolmen"              , Fixed(    Rgb([73 , 64 , 57 ]))   )
            ( 57, Flying        , "Méka"                , Fixed(    Rgb([110, 103, 142]))   )
            ( 58, Flying1       , "Métal"               , Fixed(    Rgb([82 , 79 , 90 ]))   )
            ( 59, Flying2       , "Acier"               , Fixed(    Rgb([208, 203, 183]))   )
            ( 60, Koala         , "Koala"               , Fixed(    Rgb([176, 163, 147]))   )
            ( 61, Invisible     , "Invisible"           , Invisible                         )
            ( 62, Amethyste     , "Améthyste"           , Fixed(    Rgb([193, 72 , 242]))   )
            ( 63, Emeraude      , "Emeraude"            , Fixed(    Rgb([11 , 210, 7  ]))   )
            ( 64, Rubis         , "Rubis"               , Fixed(    Rgb([210, 6  , 73 ]))   )
            ( 65, Saphir        , "Saphir"              , Fixed(    Rgb([6  , 102, 210]))   )
            ( 66, Fog           , "Brouillard"          , Fixed(    Rgb([102, 108, 133]))   )
            ( 67, Shade         , "Ombre"               , Fixed(    Rgb([50 , 55 , 76 ]))   )
            ( 68, Light         , "Lumière"             , Fixed(    Rgb([239, 217, 125]))   )
            ( 69, Gold          , "Or"                  , Fixed(    Rgb([252, 220, 45 ]))   )
            ( 70, Message       , "Forum"               , Fixed(    Rgb([108, 79 , 43 ]))   )
            ( 71, Teleport      , "Téléport"            , Fixed(    Rgb([234, 229, 229]))   )
            ( 72, Jump          , "Ressort"             , Fixed(    Rgb([190, 148, 158]))   )
            ( 73, Chest         , "Kofre"               , Fixed(    Rgb([117, 58 , 37 ]))   )
            ( 74, Multicol      , "Rubix"               , Fixed(    Rgb([124, 123, 100]))   )
            ( 75, MTwin         , "Motion Twin"         , Fixed(    Rgb([90 , 29 , 44 ]))   )
            ( 76, Crate         , "Caisse"              , Fixed(    Rgb([216, 136, 58 ]))   )
            ( 77, Noel          , "Noël"                , Fixed(    Rgb([176, 185, 243]))   )

            ( 78, Unknown       , "Inconnu"             , Fixed(    Rgb([0  , 0  , 0  ]))   )
        }

        $($input)*
    } }
}

/// Helper macro for `expand_all_blocks`.
///
/// Syntax:
/// ```rs,ignore
/// __expand_impl! {
///     items={...}
///     prefix={...}
///     repeat= <braces/brackets/parens> {...}
///     postfix={...}
/// }
/// ```
///
/// Inside the `repeat` block, `#ID`, `#NAME`, `#FR`, `#COLOR` will be replaced.
#[doc(hidden)]
macro_rules! __expand_impl {
    /*
        TT-muncher that takes processed tokens and does the actual replacement.
    */

    // Munches `copy $tok` and adds the token to the output.
    (
        @replace_tokens
        toks=[copy $tok:tt $($rest:tt)*]
        out=[$($out:tt)*]
    ) => {
        __expand_impl! { @replace_tokens toks=[$($rest)*] out=[$($out)* $tok] }
    };
    // Munches `#ID <item>` and adds the item's ID to the output.
    (
        @replace_tokens
        toks=[#ID ($id:tt, $_name:tt, $_fr:tt, $_color:expr) $($rest:tt)*]
        out=[$($out:tt)*]
    ) => {
        __expand_impl! { @replace_tokens toks=[$($rest)*] out=[$($out)* $id] }
    };
    // Munches `#NAME <item>` and adds the item's name to the output.
    (
        @replace_tokens
        toks=[#NAME ($_id:tt, $name:tt, $_fr:tt, $_color:expr) $($rest:tt)*]
        out=[$($out:tt)*]
    ) => {
        __expand_impl! { @replace_tokens toks=[$($rest)*] out=[$($out)* $name] }
    };
    // Munches `#FR <item>` and adds the item's french name to the output.
    (
        @replace_tokens
        toks=[#FR ($_id:tt, $_name:tt, $fr:tt, $_color:expr) $($rest:tt)*]
        out=[$($out:tt)*]
    ) => {
        __expand_impl! { @replace_tokens toks=[$($rest)*] out=[$($out)* $fr] }
    };
    // Munches `#COLOR <item>` and adds the item's color expression to the output.
    (
        @replace_tokens
        toks=[#COLOR ($_id:tt, $_name:tt, $_fr:tt, $color:expr) $($rest:tt)*]
        out=[$($out:tt)*]
    ) => {
        __expand_impl! { @replace_tokens toks=[$($rest)*] out=[$($out)* $color] }
    };
    // Munching is done, go to emitting the output.
    (@replace_tokens toks=[] $($out:tt)*) => {
        __expand_impl!{ @emit_output $($out)* }
    };

    /*
        TT-muncher that takes expanded `$tok <item>` pairs and filters out the `#`-identifier syntax.
    */

    // Munches a `#`-identifier pair and add it to the processed tokens,
    // together with the attached item.
    (
        @filter_pounds
        expanded=[# $_:tt $ident:ident $item:tt$($rest:tt)*]
        toks=[$($tokens:tt)*]
        $($tt:tt)*
    ) => {
        __expand_impl! {
            @filter_pounds
            expanded=[$($rest)*]
            toks=[$($tokens)* # $ident $item ]
            $($tt)*
        }
    };
    // Munches any other token, adds it to the processed tokens and discards the attached item.
    (
        @filter_pounds
        expanded=[$tok:tt $item:tt $($rest:tt)*]
        toks=[$($tokens:tt)*]
        $($tt:tt)*
    ) => {
        __expand_impl! {
            @filter_pounds
            expanded=[$($rest)*]
            toks=[$($tokens)* copy $tok]
            $($tt)*
        }
    };
    // Munching is done, go to doing replacements.
    (@filter_pounds expanded=[] toks=$toks:tt $($tt:tt)*) => {
        __expand_impl! { @replace_tokens toks=$toks $($tt)* }
    };

    /*
        Emit output, with braces, brackets or parens around the repeated part.
    */
    (@emit_output out=[{ $($pre:tt)* } { $($post:tt)* } braces $($out:tt)*]) => {
        $($pre)* { $($out)* } $($post)*
    };
    (@emit_output out=[{ $($pre:tt)* } { $($post:tt)* } brackets $($out:tt)*] ) => {
        $($pre)* [ $($out)* ] $($post)*
    };
    (@emit_output out=[{ $($pre:tt)* } { $($post:tt)* } parens $($out:tt)*] ) => {
        $($pre)* ( $($out)* ) $($post)*
    };

    /*
        Prepare extended stream by intercaling <item> before each token of the repeating part,
        then go to filtering #-syntax.
    */
    (
        @expand_repeat
        repeat=$repkind:ident [$($item:tt { $($repeat:tt)* })*]
        out=[$pre:tt $post:tt]
    ) => {
        __expand_impl! {
            @filter_pounds expanded=[$( $($repeat $item)* )*]
            toks=[]
            out=[$pre $post $repkind]
        }
    };

    /*
        Entry point.

        Duplicate the repeating part once for each item and continue with further expansion.
    */
    (
        items={$($items:tt)*}
        prefix=$pre:tt
        repeat=$repkind:ident $repeat:tt
        postfix=$post:tt
    ) => {
        __expand_impl! {
            @expand_repeat
            repeat=$repkind [$($items $repeat)*]
            out=[$pre $post]
        }
    };
}
