use std::collections::hash_map::Entry;
use std::collections::HashMap;

pub use kube_core::region::ChunkPos;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ChunkStatus {
    Generated,
    Ungenerated,
    Errored,
}

struct MissingChunk {
    // establishes an insertion order
    idx: u64,
    // adjacent chunks which are generated
    adj_chunks: u8,
}

pub struct ChunkStatusMap {
    status: HashMap<ChunkPos, ChunkStatus>,
    missing: HashMap<ChunkPos, MissingChunk>,
    next_missing_idx: u64,
}

impl ChunkStatusMap {
    pub fn new(start_chunk: ChunkPos) -> Self {
        let mut new = Self {
            status: HashMap::new(),
            missing: HashMap::new(),
            next_missing_idx: 1,
        };
        new.missing.insert(
            start_chunk,
            MissingChunk {
                idx: 0,
                // This will never reach 0, forcing this chunk
                // to be checked if unknown.
                adj_chunks: 10,
            },
        );
        new
    }

    pub fn set_status(&mut self, pos: ChunkPos, status: ChunkStatus) {
        let old = match self.status.insert(pos, status) {
            Some(old) => old,
            None => {
                self.missing.remove(&pos);
                ChunkStatus::Ungenerated
            }
        };

        let func = match (
            old == ChunkStatus::Generated,
            status == ChunkStatus::Generated,
        ) {
            (false, true) => Self::increase_present,
            (true, false) => Self::decrease_present,
            _ => return,
        };
        for p in Self::neighbors(pos).iter() {
            func(self, *p)
        }
    }

    pub fn missing_chunks(&self) -> Vec<ChunkPos> {
        let mut vec: Vec<_> = self.missing.keys().copied().collect();
        vec.sort_unstable_by_key(|e| self.missing[e].idx);
        vec
    }

    pub fn sort_missing_chunks_by<K, F>(&mut self, mut key: F)
    where
        K: Ord,
        F: FnMut(&ChunkPos) -> K,
    {
        let mut vec: Vec<_> = self.missing.iter_mut().collect();
        vec.sort_by_key(|e| key(e.0));

        self.next_missing_idx = vec.len() as u64;
        for (i, (_, e)) in vec.into_iter().enumerate() {
            e.idx = i as u64;
        }
    }

    fn neighbors(pos: ChunkPos) -> [ChunkPos; 4] {
        let (cx, cy) = pos;
        [(cx + 1, cy), (cx, cy + 1), (cx - 1, cy), (cx, cy - 1)]
    }

    fn increase_present(&mut self, pos: ChunkPos) {
        match self.missing.entry(pos) {
            Entry::Occupied(mut entry) => entry.get_mut().adj_chunks += 1,
            Entry::Vacant(entry) => {
                if self.status.get(&pos).is_none() {
                    entry.insert(MissingChunk {
                        idx: self.next_missing_idx,
                        adj_chunks: 1,
                    });
                    self.next_missing_idx += 1;
                }
            }
        }
    }

    fn decrease_present(&mut self, pos: ChunkPos) {
        if let Entry::Occupied(mut entry) = self.missing.entry(pos) {
            if entry.get_mut().adj_chunks <= 1 {
                entry.remove();
            } else {
                entry.get_mut().adj_chunks -= 1;
            }
        }
    }
}

// TODO: add tests for ChunkStatusMap
